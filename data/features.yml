- title: Reporting and Publishing
  features:
  - description: Host static pages straight (with TLS and CNAME support) from GitLab using GitLab Pages
    link: http://doc.gitlab.com/ee/pages/README.html
  - description: Contribution Analytics, see detailed statistics of contributors
    link: https://about.gitlab.com/features/#compare
  - description: Edit files, directories and create merge requests straight from the web interface
    link: http://doc.gitlab.com/ce/workflow/web_editor.html
    in_ce: true
  - description: Audit log and events
    link: http://doc.gitlab.com/ee/administration/audit_events.html

- title: Extended authentication and authorization integration
  features:
  - description: LDAP user authentication (also compatible with Active Directory)
    link: http://doc.gitlab.com/ee/integration/ldap.html#gitlab-ldap-integration
    in_ce: true
  - description: Multiple LDAP server support (also compatible with Active Directory)
    link: http://doc.gitlab.com/ee/integration/ldap.html#integrate-gitlab-with-more-than-one-ldap-server-enterprise-edition
    in_ce: false
  - description: LDAP group synchronization (also compatible with Active Directory)
    link: http://doc.gitlab.com/ee/integration/ldap.html#ldap-group-synchronization-gitlab-enterprise-edition
  - description: Create and remove admins based on an LDAP group
    link: http://doc.gitlab.com/ee/integration/ldap.html#enabling-the-admin-group-feature
  - description: Kerberos user authentication
    link: http://doc.gitlab.com/ee/integration/kerberos.html
  - description: Two-factor Authentication
    link: http://doc.gitlab.com/ce/profile/two_factor_authentication.html
    in_ce: true
  - description: Integrate with Atlassian Crowd
    link: http://doc.gitlab.com/ce/integration/crowd.html
  - description: Central Authentication Service (CAS) integration
    link: http://doc.gitlab.com/ce/integration/cas.html
    in_ce: true

- title: Fine-grained workflow management
  features:
  - description: Groups consisting of multiple people with a shared namespace for projects
    link: http://doc.gitlab.com/ce/workflow/groups.html
    in_ce: true
  - description: Ability to fork a repository
    in_ce: true
  - description: Share a project with other groups
    link: http://doc.gitlab.com/ee/workflow/share_projects_with_other_groups.html
  - description: Manage large binaries with git LFS
    link: http://doc.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html
    in_ce: true
  - description: Manage large binaries with git annex
    link: http://doc.gitlab.com/ee/workflow/git_annex.html
  - description: Rebase merge requests before merge
    link: http://doc.gitlab.com/ee/workflow/rebase_before_merge.html
  - description: Use fast-forward merges when possible
    link: http://doc.gitlab.com/ee/workflow/ff_merge.html
  - description: Git hooks (commit message must mention an issue, no tag deletion, etc.)
    link: http://doc.gitlab.com/ee/git_hooks/git_hooks.html
  - description: Webhooks at Project Level
    link: http://doc.gitlab.com/ee/web_hooks/web_hooks.html
    in_ce: true
  - description: Webhooks at Group Level  
    link: http://doc.gitlab.com/ee/web_hooks/web_hooks.html
  - description: Lock project membership to the members of a group
    link: http://doc.gitlab.com/ee/workflow/groups.html
  - description: Approve Merge Requests
    link: http://doc.gitlab.com/ee/workflow/merge_request_approvals.html
  - description: Mirror External Repositories
    link: http://doc.gitlab.com/ee/workflow/repository_mirroring.html
  - description: Automatically Merge on Build Success
    link: http://doc.gitlab.com/ce/workflow/merge_when_build_succeeds.html
    in_ce: true
  - description: Set weight of issues
    link: http://doc.gitlab.com/ee/workflow/issue_weight.html
  - description: Create templates for issues and merge requests
    link: http://doc.gitlab.com/ee/customization/issue_and_merge_request_template.html
  - description: Quick see what is important with Todos
    in_ce: true
  - description: Revert any commit quickly and easily
    link: http://doc.gitlab.com/ce/workflow/revert_changes.html
    in_ce: true

- title: Additional server management options
  features:
  - description: Branded Login Page
    link: http://doc.gitlab.com/ee/customization/branded_login_page.html
    in_ce: true
  - description: An admin can email all users of a project, a group or the entire server
    email: http://doc.gitlab.com/ee/tools/email.html
  - description: Omnibus package supports log forwarding
    link: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#udp-log-shipping-gitlab-enterprise-edition-only
  - description: Project importing from GitHub to GitLab
    link: http://doc.gitlab.com/ce/workflow/importing/import_projects_from_github.html
    in_ce: true
  - description: Project importing from GitHub Enterprise to GitLab
    link: http://doc.gitlab.com/ce/workflow/importing/import_projects_from_github.html
  - description: Project importing from GitLab.com to your private GitLab instance
    link: http://doc.gitlab.com/ce/workflow/importing/import_projects_from_gitlab_com.html
  - description: Super-powered search using Elasticsearch
    link: http://doc.gitlab.com/ee/integration/elasticsearch.html#sts=Elasticsearch_integration

- title: Deeper integration with your tool stack
  features:
  - description: Mention JIRA ticket from GitLab
    link: http://doc.gitlab.com/ce/integration/external-issue-tracker.html
    in_ce: true
  - description: Reference JIRA issues with GitLab commits, merge requests and issues
    link: http://doc.gitlab.com/ee/integration/jira.html
    in_ce: true
  - description: Close JIRA issues with GitLab commits
    link: http://doc.gitlab.com/ee/integration/jira.html
    in_ce: true
  - description: Display merge request status for builds on Jenkins CI
    link: http://doc.gitlab.com/ee/integration/jenkins.html
  - description: Omnibus package supports configuring an external PostgreSQL database
    link: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#using-a-non-packaged-postgresql-database-management-server
    in_ce: true
  - description: Omnibus package supports configuring an external MySQL database
    link: https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/README.md#using-a-mysql-database-management-server-enterprise-edition-only

- title: Continuous Integration
  features:
  - description: Continuous integration shipped along and fully integrated in the GitLab interface
    in_ce: true
    link: http://doc.gitlab.com/ce/ci/
  - description: Configure CI builds using a versioned, flexible build script
    in_ce: true
    link: http://doc.gitlab.com/ce/ci/yaml/README.html
  - description: Power your CI with Docker
    in_ce: true
    link: http://doc.gitlab.com/ce/ci/docker/using_docker_images.html
  - description: Artifacts as output. Stored in GitLab and explorable
    in_ce: true
    link: http://doc.gitlab.com/ce/ci/build_artifacts/README.html
  - description: Run your builds on any machine, architecture, infinitely scalable
    in_ce: true
    link: http://doc.gitlab.com/ce/ci/runners/README.html
  - description: Trigger builds easily, allowing for extensive customization and integration with your existing tools
    in_ce: true
    link: http://doc.gitlab.com/ce/ci/triggers/README.html
