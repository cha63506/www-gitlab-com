---
layout: markdown_page
title: Responsible Disclosure Policy
---

Please email security@gitlab.com to report any security vulnerabilities. Please
refrain from requesting compensation for reporting vulnerabilities. We will
acknowledge receipt of your vulnerability report and send you regular updates
about our progress. If you want we will [publicly acknowledge](https://about.gitlab.com/vulnerability-acknowledgements/)
 your responsible disclosure. You may also send us your report via [HackerOne](https://hackerone.com/gitlab).  


You are not allowed to search for vulnerabilities on GitLab.com itself. GitLab
is open source software, you can install a copy yourself and test against that.
If you want to perform testing that might break things please contact us to
arrange access to a staging server.


If you want to encrypt your disclosure email please email us to ask for our PGP key.
