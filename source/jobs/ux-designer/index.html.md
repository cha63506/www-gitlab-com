---
layout: markdown_page
title: "UX Designer"
---

## Responsibilities

* Create wireframes/mockups/clickable deliverables to show the transitions and interactions of new features
* Improve the interface of GitLab
* Work with developers to improve flows
* Conduct user testing
UI/UX latest techniques, prototyping with tools or HTML/CSS, user testing, user flow.

## Tools

* UI/UX: Adobe CC, Sketch, Antetype, Web typography, assets
* Prototyping: Framer, Origami by Facebook, Principal for mac, HTML/CSS/JS prototyping only.

## Workflow

You work on issues tagged with 'UX' on [CE](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=ux) and [EE](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=ux).

The priority of the issues tagged with this label can be found in [the handbook under GitLab Workflow](https://about.gitlab.com/handbook/#prioritize).

When done with an UX issue remove the UX label and add the next [workflow label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#workflow-labels) which is probably the 'Frontend' label.

## Success Criteria

You know you are doing a good job as a UX Designer when:

* You are resolving UX / UI issues assigned to milestones well before the milestone comes up.
* You communicate well with the developers.
* You are contributing ideas and solutions beyond existing issues.
* Users are overwhelmingly happy about your contributions.
* You collaborate effectively with Frontend Engineers, Developers, and Designers.

## Roles in practice

* Andriy is mostly working in close collaboration with Dmitriy on his initiatives
* Liza is mostly working on the items in the milestone and making mockups for new features
* Andriy and Liza are both learning html and css to have deliverables that are a good draft for the implementation
